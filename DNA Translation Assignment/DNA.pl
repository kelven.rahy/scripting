#!/usr/bin/perl
use strict;

open (FILE, "ACE2Sequences.fasta");
my @list;
while(my $line=<FILE>)
{
	local $/ = "\r\n";
	chomp($line);
	@list=(@list, $line);
}
my @sequence;
close FILE;
my $header;

open(MYFILE, ">text.txt");
my $count=1;
foreach my $x (@list)
{
	if ($x =~ /^>/)
	{
		$header=$x;
		my $length=@sequence;
		if ($length!=0){
		print MYFILE "Sequence #$count: $header\n\n";
		codon (@sequence);
		$count++;}
		@sequence=();
	}
	else
	{	@sequence=(@sequence, $x); }
}
print MYFILE "Sequence #$count: $header\n\n";
codon (@sequence);


sub codon
{
	my %codon= ('AAA' => 'F', 'AAG' => 'F', 'AAT' => 'L', 'AAC' => 'L', 'GAA' => 'L', 'GAG' => 'L', 'GAT' => 'L', 'GAC' => 'L',
			    'TAA' => 'I', 'TAG' => 'I', 'TAT' => 'I', 'TAC' => 'M', 'CAA' => 'V', 'CAG' => 'V', 'CAT' => 'V', 'CAC' => 'V',
			    'AGA' => 'S', 'AGG' => 'S', 'AGT' => 'S', 'AGC' => 'S', 'GGA' => 'P', 'GGG' => 'P', 'GGT' => 'P', 'GGC' => 'P',
			    'TGA' => 'T', 'TGG' => 'T', 'TGT' => 'T', 'TGC' => 'T', 'CGA' => 'A', 'CGG' => 'A', 'CGT' => 'A', 'CGC' => 'A',
			    'ATA' => 'Y', 'ATG' => 'Y', 'ATT' => '.', 'ATC' => '.', 'GTA' => 'H', 'GTG' => 'H', 'GTT' => 'Q', 'GTC' => 'Q',
			    'TTA' => 'N', 'TTG' => 'N', 'TTT' => 'K', 'TTC' => 'K', 'CTA' => 'D', 'CTG' => 'D', 'CTT' => 'E', 'CTC' => 'E',
			    'ACA' => 'C', 'ACG' => 'C', 'ACT' => '.', 'ACC' => 'W', 'GCA' => 'R', 'GCG' => 'R', 'GCT' => 'R', 'GCC' => 'R',
			    'TCA' => 'S', 'TCG' => 'S', 'TCT' => 'R', 'TCC' => 'R', 'CCA' => 'G', 'CCG' => 'G', 'CCT' => 'G', 'CCC' => 'G');
	my $sequence=join('', @_);
	my @amino=();
	for (my $i=0; $i<length($sequence); $i+=3)
{
	my $c=substr($sequence, $i, 3);
	if (length($c)<3)
	{ last; }
	my $aa= $codon{$c};
	@amino=(@amino, $aa);
}
my $seq=join(', ', @amino);
print MYFILE "The sequence of amino acids from the first reading frame is: $seq\n\n";

my @amino=();
	for (my $i=1; $i<length($sequence); $i+=3)
{
	my $c=substr($sequence, $i, 3);
	if (length($c)<3)
	{ last; }
	my $aa= $codon{$c};
	@amino=(@amino, $aa);
}
my $seq=join(', ', @amino);
print MYFILE "The sequence of amino acids from the second reading frame is: $seq\n\n";

my @amino=();
	for (my $i=2; $i<length($sequence); $i+=3)
{
	my $c=substr($sequence, $i, 3);
	if (length($c)<3)
	{ last; }
	my $aa= $codon{$c};
	@amino=(@amino, $aa);
}
my $seq=join(', ', @amino);
print MYFILE "The sequence of amino acids from the third reading frame is: $seq\n\n";
}

close (MYFILE);