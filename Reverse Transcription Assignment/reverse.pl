#!/usr/bin/perl
use strict;

my %codon= ('F' => ['AAA', 'AAG'],  'L' => ['AAT', 'AAC' ,'GAA' ,'GAG','GAT','GAC',] , 'I' => ['TAA', 'TAG', 'TAT'],
				'M' => ['TAC'], 'V' => ['CAA', 'CAG', 'CAT', 'CAC'], 'S' => ['AGA', 'AGG', 'AGT', 'AGC', 'TCA', 'TCG'],
				'P' => ['GGA', 'GGG' , 'GGT', 'GGC'],'T' => ['TGA', 'TGG', 'TGT', 'TGC'], 'A' => ['CGA', 'CGG', 'CGT', 'CGC'], 
			    'Y' => ['ATA','ATG'], '.' => ['ATT', 'ATC', 'ACT'], 'H' => ['GTA', 'GTG'], 'Q' => ['GTT', 'GTC'],
			    'N' => ['TTA', 'TTG'], 'K' => ['TTT', 'TTC'], 'D' => ['CTA', 'CTG'], 'E' => ['CTT', 'CTC'], 'C' => ['ACA', 'ACG'],
			    'W'=>['ACC'], 'R' => ['GCA', 'GCG', 'GCT', 'GCC', 'TCC', 'TCT'], 'G' => ['CCA', 'CCG', 'CCT', 'CCC']);

open (FILE, "amino_acids.txt");
my @list;
while(my $line=<FILE>)
{
	local $/ = "\r\n";
	chomp($line);
	@list=(@list, $line);
}
close FILE;
my $str=join('', @list);

my @amino_acids=split(/, /, $str);

open(MYFILE, ">Nucleotide_sequence.txt");
for (my $i=0; $i<3; $i++)
{
my @DNA=();
my $num=$i+1;
print MYFILE "Possible nucleotide #$num:\n";
foreach my $x (@amino_acids)
{
	if (scalar(@{$codon{$x}})<=$i)
	{
		@DNA = (@DNA, $codon{$x}[0]);
	}
	else
	{
		@DNA = (@DNA, $codon{$x}[$i]);
	}
}
print MYFILE @DNA;
print MYFILE "\n\n";
}


close (MYFILE);