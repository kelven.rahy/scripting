#!/usr/bin/perl

@list = (1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

@odd = @list[0,2,4,6,8];
@prime = @list[1, 2, 4, 6];

print "The list is: ";
foreach $x (@list)
{ print "$x ";}

print "\nThe odd list is: ";
foreach $x (@odd)
{ print "$x ";}

print "\nThe prime list is: ";
foreach $x (@prime)
{ print "$x ";}

$List=$#list+1;
$Odd=$#odd+1;
$Prime=$#prime+1;
print "\nSize of List: $List\nNumber of odd numbers: $Odd\nnNumber of prime numbers: $Prime.\n";