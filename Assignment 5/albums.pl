#!/usr/bin/perl

@albums=("Dear Evan Hansen, 2015, Benj Pasek", "Hamilton, 2015, Lin Manuel Miranda", "Mean Girls, 2017, Tina Fey", "Wicked, 2003, Stephen Schwartz");

print "The albums are:\n";

foreach $x (@albums)
{
  ($album, $year, $artist)=split(/,/, $x);
  print "$artist, $album\n";
}
