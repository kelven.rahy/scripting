#!/usr/bin/perl

print "Please enter a long string!\n";
$long=<STDIN>;
chomp $long;

print "Please enter a short string!\n";
$short=<STDIN>;
chomp $short;

print "Using index:";
$index=index $long, $short;
print "$index\n";

print "Using rindex:";
$index=rindex $long, $short;
print "$index\n";