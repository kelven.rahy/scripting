#!/usr/bin/perl

$animal1="koala";
$animal2="kangaroo";
$animal3="ostrich";

$num = int rand(3)+1;

print "The 3 animals are: $animal1, $animal2, and $animal3. Guess which one was chosen.\n";
$chosen=<STDIN>;
chomp $chosen;

if ($num==1)
{ $animal=$animal1; }
else
{
	if ($num==2)
	{$animal=$animal2;}
	else
	{ $animal=$animal3; }
}

if ($animal ne $chosen)
{ print "You guessed incorrectly :( The right answer was: $animal.\n";}
else
{ print "You guessed correctly :)\n";}