#!/usr/bin/perl

print "What's the temperature outside in Fahrenheit?\n";
$ans=<STDIN>;
chomp $ans;

$celsius=($ans-32)*5/9;

print "The temperature in Celsius is: $celsius\n";

if ($ans <40)
{ print "Don't forget to take a coat!!\n";}
elsif ($ans > 80)
{ print "Avoid the sunburn!!\n";}
else
{ print "It will be a great day!!\n";}