#!/usr/bin/perl

$one=int rand(6)+1;
$two=int rand(6)+1;
$three=int rand(6)+1;
$four=int rand(6)+1;
$five=int rand(6)+1;
$six=int rand(6)+1;

print "#1: " . $one;
print "\n#2: " . $two;
print "\n#3: " . $three;
print "\n#4: " . $four;
print "\n#5: " . $five;
print "\n#6: " . $six;

$total = $one + $two + $three + $four + $five + $six;

print "\nThe total is: $total\n";
if ($total>20)
{ print "You win."; }
else
{ print "You lose."; }

if ($total%2==0)
{ print "\nThe total is even.\n"; }
else
{ print "\nThe total is odd.\n"; }