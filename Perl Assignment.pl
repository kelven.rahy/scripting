#!/usr/bin/perl

# Name: Kelven Rahy
# ID: 201801014
# This perl script will generate 5 dice numbers and print their sum.

$one=int rand(6)+1;
$two=int rand(6)+1;
$three=int rand(6)+1;
$four=int rand(6)+1;
$five=int rand(6)+1;

print "#1: " . $one;
print "\n#2: " . $two;
print "\n#3: " . $three;
print "\n#4: " . $four;
print "\n#5: " . $five;

$total = $one + $two + $three + $four + $five;

print "\nThe sum of all dice is: " . $total . "\n";